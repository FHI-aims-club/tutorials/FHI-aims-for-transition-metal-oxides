# FHI-aims for Transition Metal Oxides

Simulation of transition-metal-oxide clusters with FHI-aims. Please visit the following page to read the detailed instructions:

https://fhi-aims-club.gitlab.io/tutorials/FHI-aims-for-transition-metal-oxides

## Contents

1. Finding and preparing the initial structure
2. Relaxation of the initial structure
3. Accurate Bulk Properties with HSE06: Calculation of the DOS and band structure
4. Building a stoichiometric cluster
5. Converging clusters
6. Oxygen evolution reaction
