# Charge and Spin Initialization: Complex Materials Simulations 

<!-- This tutorial is about the simulation of transition-metal-oxide clusters with FHI-aims. Specifically, we will try to simulate steps of the oxygen evolution reaction on the surface of a Fe$_2$O$_3$ cluster. -->

<!-- ## Structure of the tutorial

The structure of the tutorial is outlined in the left-side panel of this site. The idea is to follow this structure step by step. On the right side of any of this sites you will always find the table of contents for the current page. -->

Welcome to this tutorial on how to use the [FHI-aims](https://fhi-aims.org) code for simulations of chemical reactions at oxide nanostructures. 

This tutorial was developed by Sebastian Kokott, Saeed Bohloul, Sergey Levchenko and Volker Blum. It was originally created at the suggestion of the EU COST action 18234 "Computational Materials Sciences for Efficient Water Splitting with Nanocrystals from Abundant Elements." However, it can serve as a standalone tutorial for a variety of fairly complex simulation tasks, using [FHI-aims](https://fhi-aims.org) as well as several further useful software packages, including the browser-based ["Graphical Interface for Materials Science" (GIMS)](https://gims.ms1p.org).

In order to successfully execute this tutorial, you should also have access to the two primary community resources of the FHI-aims project:

- The FHI-aims development and community server <https://aims-git.rz-berlin.mpg.de>
- The FHI-aims slack workspace <https://fhi-aims.slack.com>

Any user of FHI-aims is encouraged to request access to both resources (please follow the instructions at <https://fhi-aims.org/get-the-code>).

All files related to the tutorial, including solutions, can be found at <https://gitlab.com/FHI-aims-club/tutorials/FHI-aims-for-transition-metal-oxides>.

A current copy of the FHI-aims manual can be found at <https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/jobs/artifacts/master/raw/doc/manual/FHI-aims.pdf?job=build.manual>.

And yes, FHI-aims is a community code. Without its large community of contributors, FHI-aims would not exist. We are immensely grateful to the very large number of individuals who have contributed to the code over time and who continue to push it forward.

## Key Learning Objectives Covered in this Tutorial

- Semilocal and hybrid density functional theory for molecules, solids, and clusters
  - "Geometry relaxation" (finding the nearest local minimum of the potential energy surface as a function of the atomic coordinates) for molecules, clusters and solids
  - Energy band structure and density of states calculation for a solid
  - Total-energy differences for successive, suspected metastable intermediates of a chemical reaction
- Creating physically meaningful geometry and spin states for a complex material (solid, molecule, or cluster)
  - Finding reliable experimental reference data for bulk structure and spin state of a material
  - Converting and manipulating input experimental crystallographic geometry information, including charge and spin initialization, using [GIMS](https://gims.ms1p.org)
  - Creating a compact cluster "cut-out" geometry from a periodic structure using a Command-Line Interface for Materials Science [(CLIMS)](https://gitlab.com/FHI-aims-club/utilities/clims)
- Dealing with system geometries in which s.c.f. convergence is difficult or not unique, i.e., convergence from a blind initialization would not be achievable or meaningful: 
  - Detailed charge and spin initialization of the self-consistent field (s.c.f.) cycle 
  - Addressing initial geometry guesses that are far from a chemical equilibrium state and therefore far from electronic stability in a single-reference method (DFT)

## The Objective and the Case Study

This tutorial introduces FHI-aims and its capability to simulate chemical reaction steps on transition-metal-oxide nano-clusters, through a series of hands-on tutorials. The target material of choice is ***Fe$_2$O$_3$ (iron-oxide - here, based on the hematite crystal form)***. We construct and use a model cluster of Fe$_2$O$_3$, at which we subsequently adsorb various small molecules involved in the reaction steps of the ***oxygen evolution reaction (OER)***. 

One particular reference that deals with this system in much greater detail (albeit for surfaces, not for clusters) is [Peilin Liao, John A. Keith, Emily A. Carter, "Water Oxidation on Pure and Doped Hematite (0001) Surfaces: Prediction of Co and Ni as Effective Dopants for Electrocatalysis", J. Am. Chem. Soc. 2012, 134, 13296-13309](https://dx.doi.org/10.1021/ja301567f).

For these questions, FHI-aims has an advantage in that it can deal with non-periodic nanostructures and with periodic solids and surfaces on exactly equal footing, simply by including or omitting unit cell vectors. This would, for example, allow for explicitly charged simulations of non-periodic system models that might be technically difficult to accomplish without artifacts in a periodic simulations.

Furthermore, given a reasonably powerful computer, FHI-aims can address rather large structure sizes in an achievable fashion, including with hybrid density functionals for over 1,000 atoms (again, on a large computer), all-electron and without any significant approximations in the underlying numerical implementation.

There is much more we could be doing: Vibrational corrections, reaction pathways, molecular dynamics, visualization of electronic levels, various forms of "computational spectroscopies". All this can be done with FHI-aims but is not within reach of this few-hour tutorial.

## Prerequisites

Users of this tutorial should have a sufficiently good understanding of the Unix command line. Practically every current high-performance computing architecture is operated using the command line. Any other form of access is usually restricted already due to security concerns on the part of computing centers. The command line is therefore, for better or for worse, the state-of-the-art approach to conducting computational simulations on high-performance computers.

It is similarly important to understand how to transfer files between a remote supercomputer and a local computer with a display. Graphical data visualization is an essential part of any scientific analysis. Being comfortable with data visualization at all steps of a simulation is a critical skill, without which a meaningful scientific analysis can usually not be conducted.

Users should also understand how to manipulate and extract data extraction from text files.

Access to a sufficiently powerful high-performance computer, typically more than one node, with fast interconnect (not Ethernet). Most research computing clusters fall in this category.

## This is not an easy system

- The nano-cluster structure considered here is by no means the only or even necessarily the right underlying structure of a Fe$_2$O$_3$ when compared to a given experiment. Finding such cluster stuctures is a full-time scientific occupation of its own. 
- Furthermore, we here pay practically no attention to the fact that such a cluster might be solvated or otherwise embedded in a particular environment. The reaction steps considered are gas-phase reactions, simulated in vacuo. In a more realistic simulation, the impact of the environment should be considered in some form.
- The simulations in this tutorial are carried out using a semilocal density functional (PBE). For the electronic structure of oxides and for reactions of this kind, sufficient quality of semilocal density-functional theory is anything but guaranteed. In the authors' view, at least a hybrid density functional (such as the HSE06 functional or the PBE0 functional) should be considered. FHI-aims does offer a competitively efficient and numerically precise implementations of all-electron hybrid density functional theory. However, for the purposes of a tutorial, this level of theory is currently too computationally demanding.
- The materials and reaction partners in this system are charged (ionic materials), spin-polarized (bulk hematite is an antiferromagnet), include unsaturated bonds (surfaces and reaction intermediates) and often begin far from a structure in which the atoms are "comfortable" - i.e., far from the eventual local equilibrium geometries, in which the total energy gradients ("forces") with respect to all atomic positions are zero. 

This inherent electronic complexity of the materials means that one cannot simply "push a button", ask the computer to define and solve the problem, and not worry about the details. Not only the initial geometry but also the electronic and spin initialization of the calculations are critical factors to consider. There are multiple different possible self-consistent spin states that could occur for this system, but what is worse, some spin states (e.g., a fully ferromagnetic one) will be inherently unfavorable. Calculations begun with an unfavorable initial spin configuration may simply not converge to a self-consistent solution.

The preceding aspects are consequences of the physics of the simulation, not just of the choice and settings in a particular code. A precise definition of the scientific question and a detailed understanding of the system at hand are a key prerequisites for any meaningful computational simulation, but particularly for the present ones. 

For the same reason, the computational cost for all reaction steps considered here is therefore still very considerable even when using the PBE density functional. To carry out the majority of the presented simulations comfortably, a high-performance computer is needed, not (unfortunately), e.g., a simple laptop.

## Useful Links

The contents of the tutorials and the tools that are used are accessible via the following links:

  - Tutorials gitlab repository, which contains all the documents and simulation data: 
    - <https://gitlab.com/FHI-aims-club/tutorials/FHI-aims-for-transition-metal-oxides> 
  - Utilities gitlab repository, particularly [CLIMS](https://gitlab.com/FHI-aims-club/utilities/clims): 
    - <https://gitlab.com/FHI-aims-club/utilities>
  - Detailed instructions for each tutorial can be found at:
    - <https://fhi-aims-club.gitlab.io/tutorials/FHI-aims-for-transition-metal-oxides/>
  - Graphical Interface for Materials Simulation (GIMS):
    - <https://gims.ms1p.org/static/index.html>

We note that the tools used here are general and open-source. GIMS, especially, is intended to support more than just one code since, in our view, it does solve a general problem for our community. It is built upon the [atomic simulation environment (ASE)](https://wiki.fysik.dtu.dk/ase/) and we believe that adding support for other codes will be possible with reasonable effort. A description of GIMS is published here: 

Kokott et al., (2021). GIMS: Graphical Interface for Materials Simulations. Journal of Open Source Software, 6(57), 2767. <https://doi.org/10.21105/joss.02767>

## Summary of the Tutorials

### Preparation

- How to obtain and install FHI-aims 
- How to obtain and install CLIMS, a command-line utility which is used to facilitate various aspects of preparing/initialing input files which are required for a simulation with FHI-aims 
- How to access [GIMS](https://gims.ms1p.org), a very useful graphical use interface which is used for preparation, visualization, and analyzing FHI-aims input files and output data. We strongly advise users to use GIMS throughout the tutorials to visualize the simulation tasks and workflows.
- Get ready with FHI-aims: If you are new to using FHI-aims, we recommend to study the tutorial [Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) to get used to run FHI-aims for Molecules and soldis.


### Part 1

Deals with the basics of obtaining an initial conventional crystal structure for Fe$_2$O$_3$ and use FHI-aims tools to prepare and initialize a primitive crystal structure.  

- Finding the experimentally known Fe$_2$O$_3$ polymorphs from online database. In this tutorial, we will consider the hematite structure but other variants of Fe$_2$O$_3$ are also experimentally known.
- Finding the ground state spin configuration of a magnetic material (in this case, antiferromagnetic hematite).
- How to use GIMS to create a primitive structure from an imported crystallographic information (.cif) file.
- How to assign initial spin moments using GIMS.

Skipping the step of researching what is known about the system under study would be akin to hoping that an electronic structure code might miraculously define the overall scientific question under study, not just provide an answer defined by a particular question and specific simulation input. In order to use simulations effectively for computational science, it is essential to first plan the input to a simulation with great care - particularly the positions of each and every atom in the system, as well as the possible spin state, if applicable.

### Part 2

Deals with how to obtain a relaxed structure for the primitive unit cell of Fe$_2$O$_3$ for a given density functional (here, PBE and HSE06). This relaxed unit cell will later be used as a starting point to construct Fe$_2$O$_3$ nano-clusters. 

- Pre-relaxation the primitive structure with the PBE semilocal density functional
- Post-relaxation of the primitive structure with the HSE06 hybrid density functional

While the remainder of the tutorial relies on the PBE-relaxed data, a production simulation for a scientific publication might well be based on a hybrid density functional instead.

### Part 3

Demonstrates how two important physical properties of crystalline Fe$_2$O$_3$ (primitive unit cell), i.e. electronic band-structure and density of states (DOS), are calculated using FHI-aims. 

- Calculating the electronic band structure and density of states with FHI-aims
- How to use GIMS for visualizing the output data

### Part 4

Contains the first step toward building a physically meaningful nano-cluster from the primitive unitcell, by using the Wigner-Seitz cell.

- Building a proper initial stoichiometric cluster structure using a compact, Wigner-Seitz like geometry

This is merely one practical approach out of many possible hypotheses for what are the meaningful cluster structures in this system. It is by no means the only or even the most physical approach to obtain a cluster geometry for use in simulations. When simulating nanostructures for comparison with specific experiments, a detailed understanding of experimental synthesis conditions and other a priori knowledge is essential in order to formulate meaningful hypotheses regarding the structure of a nanosystem.

### Part 5

Explains how to initialize both the geometric structure and the spin configuration of a spin-polarized initial guess for a nanocluster structure. It is especially important to recall that an initial structure guess for a nanostructure will almost entail unsaturated bonds, unfavorable spin configurations, and/or very different initial bonding motifs than those that are realized in the final structure.

A sound strategy to formulate such an initial gues is essential to enable numerically stable simulations (i.e., to even be able to identify a self-consistent solution to the electronic structure). Subsequent structure optimization steps can then transform the initial cluster structure to a chemically more reasonable final structure of the Fe$_2$O$_3$ cluster.

Three approaches will be showcased:

- Use the initial structure as it is, including all unsaturated bonds.
- Remove unsaturated oxygen atoms
- Use hydrogen atoms to saturate the unsaturated oxygen atoms

### Part 6

Explains how to use the relaxed cluster structure to simulate the steps of the oxygen evolution reaction (OER) on the surface of a Fe$_2$O$_3$ cluster, and calculate energy differences between different metastable reaction intermediaries.

- Set up the steps of the oxygen evolution reaction (OER) on Fe$_2$O$_3$ cluster.
- Perform a simulation series in which the geometries build sequentially on one another.
- Perform a simulation series in which geometries for each metastable intermediary are constructed from scratch.

## Background and References

FHI-aims is an all-electron electronic structure code that uses localized, numerically tabulated atom-centered basis functions to discretize the orbitals and wave functions of electronic structure theory. This choice enables a high-precision representation of the orbitals and density of complex nano-structures with reasonable computational effort across the periodic table, including all core and valence electrons (no shape approximations to potentials or wave functions). 

Apologies that this list of references became fairly long. And yet, it is only a fairly lopsided fraction of the references we should be listing.

- The construction of basis sets for density-functional theory and the elementary numerical choices made in the implementation are summarized here:

[1] Volker Blum, Ralf Gehrke, Felix Hanke, Paula Havu, Ville Havu, Xinguo Ren, Karsten Reuter, and Matthias Scheffler,
Ab initio molecular simulations with numeric atom-centered orbitals.
Computer Physics Communications 180, 2175-2196 (2009). <http://dx.doi.org/10.1016/j.cpc.2009.06.022>

Importantly, this reference includes the numerical definition of the specific approach to scalar relativistic calculations championed in FHI-aims, i.e., the FHI-aims specific variant of the "atomic zero-order regular approximation (ZORA)". We also note that the methods and concepts summarized in Ref. [1] build heavily on past published methods in our community, particularly the pioneering works of Axel Becke and Bernard Delley.

- Numerical real-space integration in FHI-aims:

[2] Ville Havu, Volker Blum, Paula Havu, and Matthias Scheffler,
Efficient O(N) integration for all-electron electronic structure calculation using numerically tabulated basis functions.
Journal of Computational Physics 228, 8367-8379 (2009). <http://dx.doi.org/10.1016/j.jcp.2009.08.008>

- Basics of handling the two-electron Coulomb operator for exact exchange and correlated methods (GW, RPA) in FHI-aims:

[3] Xinguo Ren, Patrick Rinke, Volker Blum, Jürgen Wieferink, Alex Tkatchenko, Andrea Sanfilippo, Karsten Reuter, and Matthias Scheffler,
Resolution-of-identity approach to Hartree-Fock, hybrid density functionals, RPA, MP2, and GW with numeric atom-centered orbital basis functions.
New Journal of Physics 14, 053020 (2012). <http://stacks.iop.org/1367-2630/14/053020>

- Linear-scaling hybrid density functional theory, including in periodic systems:

[4] Arvid Ihrig, Jürgen Wieferink, Igor Ying Zhang, Matti Ropo, Xinguo Ren, Patrick Rinke, Matthias Scheffler, and Volker Blum
Accurate localized resolution of identity approach for linear-scaling hybrid density functionals and for many-body perturbation theory.
New Journal of Physics 17, 093020 (2015). <http://dx.doi.org/10.1088/1367-2630/17/9/093020>

[5] Sergey Levchenko, Xinguo Ren, Jürgen Wieferink, Rainer Johanni, Patrick Rinke, Volker Blum, Matthias Scheffler
Hybrid functionals for large periodic systems in an all-electron, numeric atom-centered basis framework.
Computer Physics Communications 192, 60-69 (2015). <http://dx.doi.org/10.1016/j.cpc.2015.02.021>

- Stress tensor for periodic systems:

[6] Franz Knuth, Christian Carbogno, Viktor Atalla, Volker Blum and Matthias Scheffler
All-electron Formalism for Total Energy Strain Derivatives and Stress Tensor Components for Numeric Atom-Centered Orbitals.
Computer Physics Communications 190, 33-50 (2015). <http://authors.elsevier.com/sd/article/S0010465515000090>

- Spin-orbit coupling across the periodic table:

[7] William P. Huhn, V. Blum
One-hundred-three compound band-structure benchmark of post-self-consistent spin-orbit coupling treatments in density functional theory.
Physical Review Materials 1, 033803 (2017). <https://doi.org/10.1103/PhysRevMaterials.1.033803>

- Bethe-Salpeter Equation for neutral excitations in molecules:

[8] Chi Liu, Jan Kloppenburg, Yi Yao, Xinguo Ren, Heiko Appel, Yosuke Kanai, Volker Blum
All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals.
The Journal of Chemical Physics, 152, 044105 (2020).

- Periodic GW for quasiparticle excitations:

[9] Xinguo Ren, Florian Merz, Hong Jiang, Yi Yao, Markus Rampp, Hermann Lederer, Volker Blum and Matthias Scheffler
All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: algorithm and benchmarks.
Physical Review Materials 5, 013807 (2021). <https://doi.org/10.1103/PhysRevMaterials.5.013807> 

- High-performance eigenvalue and density matrix solutions using the ELPA eigenvalue solver and the ELSI electronic structure infrastructure: 

[10] Andreas Marek, Volker Blum, Rainer Johanni, Ville Havu, Bruno Lang, Thomas Auckenthaler, Alexander Heinecke, Hans-Joachim Bungartz, and Hermann Lederer,
The ELPA Library - Scalable Parallel Eigenvalue Solutions for Electronic Structure Theory and Computational Science
The Journal of Physics: Condensed Matter 26, 213201 (2014). <http://stacks.iop.org/0953-8984/26/213201>

[11] Hans-Joachim Bungartz, Christian Carbogno, Martin Galgon, Thomas Huckle, Simone Köcher, Hagen-Henrik Kowalski, Pavel Kus, Bruno Lang, Hermann Lederer, Valeriy Manin, Andreas Marek, Karsten Reuter, Michael Rippl, Matthias Scheffler, Christoph Scheurer,
ELPA: A parallel solver for the generalized eigenvalue problem.
I. Foster et al. (Eds.), Parallel Computing: Technology Trends, 647-668 (IOS Press, 2020).
<https://pure.mpg.de/pubman/item/item_3221243_2/component/file_3221686/APC-36-APC200095.pdf>

[12] Victor Wen-zhe Yu, Fabiano Corsetti, Alberto Garcia, William P. Huhn, Mathias Jacquelin, Weile Jia, Björn Lange, Lin Lin, Jianfeng Lu, Wenhui Mi, Ali Seifitokaldani, Alvaro Vazquez-Mayagoitia, Chao Yang, Haizhao Yang, Volker Blum
ELSI: A Unified Software Interface for Kohn-Sham Electronic Structure Solvers.
Computer Physics Communications 222, 267-285 (2018), DOI: 10.1016/j.cpc.2017.09.007 . <https://doi.org/10.1016/j.cpc.2017.09.007>

[13] Victor Wen-zhe Yu, Carmen Campos, William Dawson, Alberto García, Ville Havu, Ben Hourahine, William P Huhn, Mathias Jacquelin, Weile Jia, Murat Keçeli, Raul Laasner, Yingzhou Li, Lin Lin, Jianfeng Lu, Jonathan Moussa, Jose E Roman, Álvaro Vázquez-Mayagoitia, Chao Yang, Volker Blum
ELSI--An Open Infrastructure for Electronic Structure Solvers.
Computer Physics Communications 256, 107459 (2020). <https://dx.doi.org/10.1016/j.cpc.2020.107459>

[14] Victor Wen-zhe Yu, Jonathan Moussa, Pavel Kůs, Andreas Marek, Peter Messmer, Mina Yoon, Hermann Lederer, Volker Blum
GPU-Acceleration of the ELPA2 Distributed Eigensolver for Dense Symmetric and Hermitian Eigenproblems.
Computer Physics Communications 262, 107808 (2021). <https://doi.org/10.1016/j.cpc.2020.107808>

- GPU acceleration of FHI-aims:

[15] William Huhn, Björn Lange, Victor Wen-zhe Yu, Mina Yoon, Volker Blum
GPGPU Acceleration of All-Electron Electronic Structure Theory Using Localized Numeric Atom-Centered Basis Functions
Computer Physics Communications 254, 107314 (2020). <https://doi.org/10.1016/j.cpc.2020.107314>

- FHI-aims geometry.in file format specification:

[16] Volker Blum. FHI-aims File Format Description: geometry.in. figshare (2020). Online resource. <https://doi.org/10.6084/m9.figshare.12413477.v1> 

- GIMS:

[17] Sebastian Kokott, Iker Hurtado, Christian Vorwerk, Claudia Draxl, Volker Blum, Matthias Scheffler.
GIMS: Graphical Interface for Materials Simulations. Journal of Open Source Software, 6(57), 2767. <https://doi.org/10.21105/joss.02767>

