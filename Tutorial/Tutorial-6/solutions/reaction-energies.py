#!/usr/bin/python

#############
# Functions #
#############
def print_reaction_info(chemical_reaction, reaction_energy):
    print("{:50s} : dE = {:.4f} eV".format(chemical_reaction, reaction_energy))

def calculate_reaction_energies(cluster_bare_start,
                                cluster_bare_end,
                                cluster_plus_OH2, 
                                cluster_plus_OH,
                                cluster_plus_O,
                                cluster_plus_OOH,
                                H2O,
                                O2,
                                H2):        
    reaction_a = cluster_plus_OH2 - cluster_bare_start - H2O
    reaction_b = cluster_plus_OH  + .5 * H2 - cluster_plus_OH2
    reaction_c = cluster_plus_O   + .5 * H2 - cluster_plus_OH
    reaction_d = cluster_plus_OOH + .5 * H2 - cluster_plus_O - H2O
    reaction_e = cluster_bare_end + O2 + .5 * H2 - cluster_plus_OOH
    cumulative = reaction_a + reaction_b + reaction_c + reaction_d + reaction_e 
    electrochemical_reaction_potential = cumulative / 4

    chemical_reaction = "(A) H2O  + (*) --> *OH2"
    print_reaction_info(chemical_reaction, reaction_a)
    chemical_reaction = "(B) *OH2       --> *OH  + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_b)
    chemical_reaction = "(C) *OH        --> *O   + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_c)
    chemical_reaction = "(D) H2O  + *O  --> *OOH + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_d)
    chemical_reaction = "(E) *OOH       --> (*)  + O2 + H+ + e-"
    print_reaction_info(chemical_reaction, reaction_e)
    chemical_reaction = "Cumulative reaction energy"
    print_reaction_info(chemical_reaction, cumulative)
    chemical_reaction = "Predicted electrochemical reaction potential"
    print_reaction_info(chemical_reaction, electrochemical_reaction_potential)  
    print 


def read_molecule_energies(file_path):
    """
    Reads the molecule energy values from a file and returns them as a dictionary.

    Parameters:
        file_path (str): The path to the file containing the molecule energies.

    Returns:
        dict: A dictionary with molecule names as keys and their energies as values.
    """
    molecule_data = {}

    # Open and read the file
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Process each line
    for line in lines:
        # Split the line into molecule name and energy value
        name, value = line.split()
        # Store the value in the dictionary, converting to float
        molecule_data[name] = float(value)

    return molecule_data

def read_cluster_energies(file_path):
    """
    Reads the cluster energy values from a file and returns them as a dictionary.

    Parameters:
        file_path (str): The path to the file containing the cluster energies.

    Returns:
        dict: A dictionary with cluster names as keys and their energies as values.
    """
    cluster_data = {}

    # Open and read the file
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Process each line
    for line in lines:
        # Split the line into cluster name and energy value
        name, value = line.split()
        # Store the value in the dictionary, converting to float
        cluster_data[name] = float(value)

    return cluster_data


############################
# Molecules total energies #
############################
mol_energies = read_molecule_energies('total-energy.molecules')
H2 = mol_energies.get('H2')
H2O = mol_energies.get('H2O')
O2 = mol_energies.get('O2')

############################
# Water splitting reaction #
############################
water_splitting_energy = O2 + 2 * H2 - 2 * H2O
electrochemical_reaction_potential = water_splitting_energy / 4

print("========================================")
print("Water splitting reaction energy from DFT")
print("========================================")
chemical_reaction = "2H2O --> O2 + 4H+ + 4e-"
print_reaction_info(chemical_reaction, water_splitting_energy)
chemical_reaction = "Predicted electrochemical reaction potential"
print_reaction_info(chemical_reaction, electrochemical_reaction_potential)
print

#############################
# Approach-1 total energies #
#############################
cluster_energies = read_cluster_energies('total-energy.cluster-approach-1') 
cluster_bare_start = cluster_energies.get('step-0-bare-cluster') 
cluster_plus_OH2   = cluster_energies.get('step-1-cluster-plus-OH2')
cluster_plus_OH    = cluster_energies.get('step-2-cluster-plus-OH') 
cluster_plus_O     = cluster_energies.get('step-3-cluster-plus-O')
cluster_plus_OOH   = cluster_energies.get('step-4-cluster-plus-OOH')
cluster_bare_end   = cluster_energies.get('step-5-bare-cluster')

# Reactions
print("===============================")
print("Reaction energies of Approach 1")
print("===============================")
calculate_reaction_energies(cluster_bare_start,
                            cluster_bare_end,
                            cluster_plus_OH2, 
                            cluster_plus_OH,
                            cluster_plus_O,
                            cluster_plus_OOH,
                            H2O,
                            O2,
                            H2)

#############################
# Approach-2 total energies #
#############################
cluster_energies = read_cluster_energies('total-energy.cluster-approach-2') 
cluster_bare_start = cluster_energies.get('step-0-bare-cluster') 
cluster_plus_OH2   = cluster_energies.get('step-1-cluster-plus-OH2')
cluster_plus_OH    = cluster_energies.get('step-2-cluster-plus-OH') 
cluster_plus_O     = cluster_energies.get('step-3-cluster-plus-O')
cluster_plus_OOH   = cluster_energies.get('step-4-cluster-plus-OOH')
cluster_bare_end   = cluster_energies.get('step-5-bare-cluster')

# Reactions
print("===============================")
print("Reaction energies of Approach 2")
print("===============================")
calculate_reaction_energies(cluster_bare_start,
                            cluster_bare_end,
                            cluster_plus_OH2, 
                            cluster_plus_OH,
                            cluster_plus_O,
                            cluster_plus_OOH,
                            H2O,
                            O2,
                            H2)
