# Part 3: Bulk Properties with the HSE06 Hybrid Density Functional: Energy Band Structure and Density of States

### Objective

We now have properly relaxed the atomic and electronic structure of hematite Fe$_2$O$_3$ and can analyze the electronic structure and spin state in more detail. Specifically, we will focus on the electron band structure, local spin moment, partial and full densities of states as calculated from the DFT-HSE06 effective single-particle eigenstates, using "light" settings. As we noted before, a better set of species_defaults ("intermediate" or better) should be used for such a calculation.

However, before we launch into the discussion, we need to clarify a few fundamental issues. While we can calculate a band structure and density of states using the DFT-HSE06 density functional, it is no a priori clear if and how this data will correspond to any experimentally measurable observable. We need to understand the physics of what we are doing before we can move on to the technical aspects of the calculation.

### Current Experiment and Theory for the Fundamental Band Gap of Fe$_2$O$_3$

A recent (2019) paper by Piccinin (<https://doi.org/10.1039/C8CP07132B>) provides a summary of the state of knowledge for bulk hematite Fe$_2$O$_3$, including state of the art theory and its connection to experimental data.

The crux is that there are several different experimental observables that we might like to simulate:

  - Neutral, including optical, electronic excitations of the system. A subset of these excitations will be measurable in optical spectroscopies, for instance, absorption - reflected most effectively by the color of the material. Piccinin (whose work is computational) settles for an experimental optical absorption onset of approximately 2 eV. The actual quoted value is somewhat higher (2.14-2.20 eV) but in this author's view, the raw experimental data (reproduced in Figure 10 of Piccinin in 2019) appear to leave some room for discussion. However, a critical observation is that the absorption onset corresponds to the so-called optical gap of the system, that is, to the lowest-energy optically active neutral excitations of the system. In DFT-HSE06 (the subject of this tutorial), we do not approximate the optical gap. At best, we approximate the fundamental gap of the system (ionization potential minus electron affinity), see next point.

  - The fundamental gap of a molecule or solid is defined as the difference between the ionization potential (IP) and the electron affinity (EA) of the system - i.e., the difference between (definition of IP:) the lowest energy required to remove an electron from an initially neutral crystal and (definition of EA:) the highest energy that can be gained by attaching an electron to an initially electronically neutral crystal. Piccinin quotes an experimental value of approximately 2.6 eV for the experimentally determined, fundamental gap. (Table 1) 
  
For reasons given below, one might hope to simulate an approximation to the fundamental gap of Fe$_2$O$_3$ using the single-particle eigenvalues of the HSE06 density functional. However, let us first assess what is the experimental situation regarding the fundamental gap of hematite Fe$_2$O$_3$.
 
  - The experimental fundamental gap value of ~2.6 eV as quoted by Piccinin is taken from a 1999 paper, Zimmermann et al. <https://doi.org/10.1088/0953-8984/11/7/002>. 
 
  - In Figure 15 of Zimmermann et al., photoemission data (IP) and inverse photoemission, also known as Bremsstrahlung isochromat spectroscopy (BIS) data are shown, substantiating (visually, without the deeper analysis presumably made in that work) an approximate difference of 2-3 eV between the IP and the EA. This data is, in this authors' view, as good as it gets when trying to estimate fundamental gaps from electron spectroscopies. One remaining issue is, however, that these measurements are surface measurements, obtained by charging an otherwise insulating single-crystal sample. The impact of the surface structure after preparation of the single crystal in vacuum is not clear to this author, but it could be substantial, particularly for the experimentally derived EA.

  - The experimental fundamental gap of Fe$_2$O$_3$ of Zimmermann et al. is further substantiated by a range of literature values, 2.0-2.7 eV, attributed to Ref. 80 of Zimmermann et al. This Ref. 80 is a 1986 photoemission study by Fujimori et al. <https://doi.org/10.1103/PhysRevB.34.7318>. However, crucially, the measured data in this paper pertain only to photoemission (i.e., to the IP), but no experimental data is provided for electron attachment (EA). Their quoted fundamental gap range is traced backwards to two temperature-dependent conductivity measurements, Ref. 31 of Fujimori et al. 

- Piccinin's own paper does report computational results for the fundamental gap of Fe$_2$O$_3$ ontained by the GW approximation in various flavors. In short, "GW" is an approximation to the actual quasiparticle equation, i.e., it is based on an equation that incorporates the right physics for the quasiparticle gap, which we are hoping to approximate. While "GW" typically the "best one can do" for single-particle like quasiparticle excitations in electronic structure theory of solids, the method does depend on several further detailed approximations, such as: (i) The underlying density functional used to calculate the Green's function and the screened Coulomb interaction, (ii) non-selfconsistent or self-consistent computation of the GW self-energy, (iii) the absence of a "vertex function", which covers the local correlation and which is already approximated to be unity within "GW". Each of these approximations could be critical and Piccinin shows that different, ostensibly choices for (i)-(ii) can lead to a significant variation of GW-predicted fundamental gaps for Fe$_2$O$_3$, between 1.44 eV and 5.05 eV. Piccinin eventually settles for a non-GW treatment as the empirically "best" approach: a parameterized hybrid density functional (PBEh with an exchange mixing parameter of 1/7). 

- The presence of d-electron correlation in Fe adds some significant complexity to all of this, a discussion not pursued here, as does the absence of electron-phonon renormalization (i.e., impact of nuclear motion and relaxation on the experimental quasiparticle properties).

- Our spin state is the expected zero Kelvin spin state. It turns out that hematite has a spin phase transition at around 250 K, which could affect the observed experimental gap. Within scalar relativistic DFT, however, we cannot simulate this phase transition, since the room-temperature spin arrangement is "canted", i.e., the local spin moments found on the Fe ions are not collinear. Scalar relativistic DFT unfortunately, only allows for direction-less spin arrangements (called "up" or "down" but not coupled to the lattice). We would need a better treatment (spin-orbit coupled) to obtain an idea of the canted spin arrangement.

This is the backdrop before which the band structure predictions attempted here must be considered. 

### Why DFT-HSE06 for Band Structure Predictions of Fe$_2$O$_3$?

In general, DFT eigenvalues (including those of the HSE06 hybrid density functional, used below) do not have a formal justification - except as approximate sources of information on the ionization potential (electron removal energy) and the electron affinity (electron attachment). Both statements follow from Janak's theorem (which holds for exact density functional theory, not approximate density functionals). 

In practice, an approximate density functional must have a mathematically discontinuous derivative as a function of particle number at an integer electron number in order to be able to predict both IP and EA in the form of highest occupied and lowest unoccupied eigenvalues of the same, neutral DFT calculation. The discontinuity is not the only condition (the functional should also be linear as a function of particle number, between different integer particle numbers, and it should have the right slope).

In brief, the HSE06 functional (and any hybrid density functional) has a derivative discontinuity at integer particle numbers by construction. This sets hybrid density functionals apart from semilocal functionals, such as PBE. Thus, we can in principle use an approximate hybrid density functional as a starting point of a qualitative prediction of the fundamental gap and perhaps of other properties of the electronic quasiparticle band structure. Piccinin's work ultimately follows this approach (using PBEh, not HSE06) and simultaneously shows that one must nevertheless pay attention to whether an actual density functional parameterization can be expected to yield reasonable values for the predicted fundamental gap.  

Indeed, it is this authors' (VB) viewpoint that, for materials with a rough fundamental band gap range (1.5-3.0 eV) here attributed to Fe$_2$O$_3$, an non-materials specific calculation by the HSE06 density functional with fixed parameters (screening parameter 0.11 (Bohr radii)$^{-1}$, exchange parameter 0.25) has a reasonable chance of predicting the fundamental gap within a range of several tenths of an eV. 

In fact, in some more detailed work on sulfides and on halide perovskites in VB's group, the fundamental band gap is typically somewhat underestimated, not overestimated, by the HSE06 density functional with the above, fixed parameters (see, e.g., Refs. <https://dx.doi.org/10.1021/acs.chemmater.8b03380>, <http://dx.doi.org/10.1021/acs.chemmater.7b02638>, <https://dx.doi.org/10.1021/acs.chemmater.8b03380>, <https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.146401>, <https://pubs.acs.org/doi/abs/10.1021/jacs.9b02909>, <https://doi.org/10.1021/acs.chemmater.9b05107> and references therein for such examples). As one advantage, the choice of the HSE06 functional with fixed parameters avoids a materials-specific fit. On the other hand, the presence of the transition metal Fe introduces aspects of the electronic structure (d-electron correlation) that may not follow the trends observed in electronically simpler semiconductors and it is well known that the redox levels of transition metal ions can pose significantly larger problems.

### Tasks

In order to calculate an approximate HSE06 energy band structure, here using "light" settings for the tutorial, we proceed as follows (see `solutions` folder for illustrations of the expected output).

1. Create a new folder `bands_and_dos`.
2. Preparation of `geometry.in`: We will use the the `geometry.in.next.step` from the final HSE06 relaxation of Part 2. Remember that this will not have the `initial_moment` and `initial_charge` values inside, so like in Part 2 we will use the `clims-reinitialize-geometry ` command in combination with the `geometry.in` and `geometry.in.next.step` files from the HSE06 relaxation. Move the resulting `geometry-reinitialized.in` file to the `bands_and_dos` folder and name it `geometry.in`.
3. In the construction of `control.in` for the band structure and DOS calculation, several options are available, including editing at the command line, graphical setup in GIMS, or command line setup in CLIMS. We recommend to try the "Band Structure workflow" in GIMS.
4. We will need to choose a particular k-space path to plot the energy band structure, which we will set up using GIMS or CLIMS. Both utilities rely on the standardized k-space paths for different space groups as defined by Setyawan and Curtarolo (<https://doi.org/10.1016/j.commatsci.2010.05.010>). In other words, the choice of k-space path for the band structure calculation depends on the crystal having the right space group. However, if you check the symmetry and the Bravais lattice of the relaxed structure from Part 2, you will find that the geometry relaxation has slightly changed the structure and, thus, slightly altered the space group. To get back the space group of the hematite structure, we must ensure that GIMS and CLIMS recognize it correctly and discard small deviations. You can either do this using GIMS, by changing the `symmetry threshold` to `1e-2` in the settings (upper right corner) and, then, press the button **Primitive**. Alternatively, you can do it with clims by the following command: `clims-unit-cell-info --primitive --symthresh 1e-2`.
5. Preparation of `control.in`: If you did not use GIMS, you may use `clims-prepare-run --hse06 --bands --dos`. Again, to save computer time for this tutorial, we only use *light* species defaults. For publishing results we recommend to use *intermediate* settings at least.
6. As mentioned in Part 2 of this tutorial, if you want to just restart an earlier HSE06 calculation from its converged electronic structure, you could store density matrices for each k-point in the earlier step (many of them for this small structure), then copy these stored density matrices over to the present working directory and add the appropriate restart keyword to `control.in` also here: `elsi_restart read_and_write 100`. For larger hybrid DFT calculations with fewer k-points, this strategy is decidedly useful. For the present example, a restart may not be essential.
7. As also mentioned in Part 2 of this tutorial, for a better analysis of spin state we can use a Mulliken projection via the `output mulliken` keyword.
8. After the calculation of band structure and density of states is complete, visualize the results either with `clims-aimsplot` or upload them to [**GIMS**](https://gims-dev.ms1p.org/static/index.html#).
 
As a consistency check, the `control.in` format for this step may end up looking as follows:

    xc                                 hse06 0.11
    hse_unit                           bohr
    hybrid_xc_coeff                    0.25
    relativistic                       atomic_zora scalar
    k_grid                             8 8 8
    output                             dos_tetrahedron -20 10 3001
    output                             species_proj_dos_tetrahedron -20 10 3001
    output                             band   0.00000  0.00000  0.00000   0.50000  0.00000  0.00000   23 G  L  
    output                             band   0.50000  0.00000  0.00000   0.50000  0.23420 -0.23420   18 L  B1 
    output                             band   0.76580  0.50000  0.23420   0.50000  0.50000  0.50000   20 B  Z  
    output                             band   0.50000  0.50000  0.50000   0.00000  0.00000  0.00000   21 Z  G  
    output                             band   0.00000  0.00000  0.00000   0.36710  0.00000 -0.36710   27 G  X  
    output                             band   0.63290  0.36710  0.00000   0.50000  0.50000  0.00000   10 Q  F  
    output                             band   0.50000  0.50000  0.00000   0.63290  0.63290  0.23420    8 F  P1 
    output                             band   0.63290  0.63290  0.23420   0.50000  0.50000  0.50000   17 P1 Z  
    output                             band   0.50000  0.00000  0.00000   0.76580  0.36710  0.36710   14 L  P  
    output                             mulliken
    spin                               collinear
    [....]

Important new keywords:

- A single `output band [...]` line requests energy band structure output along a path between two points in reciprocal space. The full k-space path is set up by GIMS and can also be visualized there.

- `output dos_tetrahedron -20 10 3001` writes the density of states, integrated using the tetrahedron method, in a particular energy interval: here, between -20 and +10 eV in internal energy units of FHI-aims - typically, the Fermi level in periodic FHI-aims calculations is a few eV away from this internal zero. The number of energy output points is 3001, i.e., one point for each 0.01 eV of the energy axis. Be sure to choose sufficiently many output energy points to obtain a reasonably finely resolved density of states.

- `output species_proj_dos_tetrahedron -20 10 3001` creates a projected density of states, resolved into different species (typically, chemical elements) as defined in `control.in`, as well as spin and angular momentum channels. The keyword follows the same logic as the density of states. 

- `output mulliken` creates an additional output file `Mulliken.out` that displays the Mulliken analysis of the occupation of each atom, for each eigenvalue and each k-point. However, additionally, this keyword will write a much briefer Mulliken decomposition of qualitative atomic partial charges and spin states in the `aims.out` file - see there. The `Mulliken.out` file is not included in the `solutions` folder as it is quite large.

### Results

The results for the corresponding band structure and DOS can be again found in the `solutions` folder.

In addition to the band structure and DOS, we learn the following important predicted quantities (towards the end of `aims.out` file, last s.c.f. iteration):
```
What follows are estimated values for band gap, HOMO, LUMO, etc.
| They are estimated on a discrete k-point grid and not necessarily exact.
| For converged numbers, create a DOS and/or band structure plot on a denser k-grid.

Highest occupied state (VBM) at     -8.36919888 eV (relative to internal zero)
| Occupation number:      1.00000000
| K-point:       1 at    0.000000    0.000000    0.000000 (in units of recip. lattice)
| Spin channel:        2

Lowest unoccupied state (CBM) at    -5.29796231 eV (relative to internal zero)
| Occupation number:      0.00000000
| K-point:      16 at    0.000000    0.250000    0.625000 (in units of recip. lattice)
| Spin channel:        2

The gap value is above 0.2 eV. Unless you are using a very sparse k-point grid,
this system is most likely an insulator or a semiconductor.
ESTIMATED overall HOMO-LUMO gap:      3.07123657 eV between HOMO at k-point 1 and LUMO at k-point 16
| This appears to be an indirect band gap.
| Smallest direct gap :      3.22020790 eV for k_point 41 at    0.125000    0.125000    0.000000 (in units of recip. lattice)
| Direct gap HOMO spin channel :     1
| Direct gap LUMO spin channel :     1
```

So, the s.c.f. k-space grid provides us with an estimate of the band gap of 3.22 eV - somewhat higher than the experimentally conjectured value, but, in this author's opinion, actually not unreasonably high. The reference by Piccinin shows that it will be hard to arrive at a single, first-principles predicted value for hematite Fe$_2$O$_3$ with standard methods, without resorting to experimental data for calibration. 

There is a second band gap estimate in `control.in`, this one derived from only the k-points on the requested band structure paths. If the valence band maximum (VBM) and conduction band minimum (CBM) were both part of the plotted band structure, this band gap value would be authoritative and the band gap would have to be smaller than the gap estimated on the s.c.f. k-space grid, which is indeed the case here:
```
"Band gap" of total set of bands:
| Lowest unoccupied state:      -5.30316420 eV
| Highest occupied state :      -8.36919888 eV
| Energy difference      :       3.06603468 eV
```
  
The charge and spin analysis of the atoms in the structure looks as follows:
```
Performing Mulliken charge analysis on all atoms.
Full analysis will be written to separate file 'Mulliken.out'.
Summary of the per-atom charge analysis:
|
|  atom       electrons          charge             l=0             l=1             l=2             l=3
|     1       24.608876        1.391124        6.162855       12.390374        6.000997        0.054650
|     2       24.608875        1.391125        6.162855       12.390374        6.000997        0.054650
|     3       24.608874        1.391126        6.162854       12.390374        6.000997        0.054650
|     4       24.608875        1.391125        6.162854       12.390374        6.000997        0.054650
|     5        8.927866       -0.927866        3.853094        5.064219        0.010554
|     6        8.927192       -0.927192        3.852855        5.063784        0.010552
|     7        8.927192       -0.927192        3.852855        5.063784        0.010552
|     8        8.927866       -0.927866        3.853094        5.064219        0.010554
|     9        8.927192       -0.927192        3.852855        5.063784        0.010552
|    10        8.927192       -0.927192        3.852855        5.063784        0.010552
|
| Total      152.000000        0.000000

Summary of the per-atom spin analysis:
|
|  atom   spin      l=0         l=1         l=2         l=3
|     1    4.281562    0.024242    0.042410    4.215831   -0.000921
|     2   -4.281562   -0.024241   -0.042411   -4.215831    0.000921
|     3    4.281562    0.024242    0.042411    4.215831   -0.000921
|     4   -4.281562   -0.024242   -0.042411   -4.215831    0.000921
|     5   -0.000000   -0.000000   -0.000000   -0.000000
|     6   -0.000098    0.000001   -0.000098   -0.000001
|     7    0.000098   -0.000001    0.000098    0.000001
|     8    0.000000   -0.000000    0.000000    0.000000
|     9    0.000098   -0.000001    0.000098    0.000001
|    10   -0.000098    0.000001   -0.000098   -0.000001
|
| Total    0.000000
```

Atomic charges from a Mulliken analysis are not expected to be equal to chemical charges. The fact that the sign and trend is right is good.

However, the spin state and spin moment is remarkably close to literature values (4.28, denoting the difference between spin-up and spin-down electron count attributed to a given atom). We did retain the accepted spin state.

Finally, here are the calculated energy band structure and densities of states. By the way, the FHI-aims band structure and DOS output files are all column formatted ASCII files, so the data can also be read into any 2D plotting program for further analysis there.

The choice of the energy zero in the band structure plot for a system with a gap is arbitrary, by the way. It has no physical significance - all it does is place the zero somewhere in the gap (between occupied and unoccupied states). **Specifically, the choice of this plotted energy zero between different structures with a gap must NEVER be interpreted as a relative shift of band structures.** For proper aligmnent of the energy levels of different materials (if that can be done), it is necessary to identify and analyze a well defined internal reference level of the structure. 

![](./solutions/bands_and_dos/aimsplot.png)

The DOS has quite a 'spiky' appearance, which is well known to occur with the tetrahedron method. We can obtain a smoother DOS using Gaussian broadening instead of the tetrahedron method, by replacing the keywords
```
    output                             dos_tetrahedron -20 10 3001
    output                             species_proj_dos_tetrahedron -20 10 3001
```
with
```
    output                             dos -20 10 3001 0.05
    output                             species_proj_dos -20 10 3001 0.05
```
where the 0.05 is the standard deviation of the Gaussian broadening (in eV). We can also obtain a more converged DOS by computing it on a denser k-grid in a post-SCF step via Fourier interpolation. For this, we can add the keyword
```
dos_kgrid_factors 3 3 3
```
which makes the k-grid upon which the DOS is calculated denser by a factor of 3 in each direction. For further details on converging the DOS, please see [Basics of running FHI-aims (Part 3)](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/3-Periodic-Systems/#dos-convergence). In the present case, this requires a modest calculation expense, with the result inside `solutions/bands_and_dos_kgrid_factors` and the plot shown below.

![](./solutions/bands_and_dos_kgrid_factors/aimsplot.png)