# Part 2: Relaxation of the initial structure

In this tutorial, we use the experimental structure of bulk Hematite Fe$_2$O$_3$ as a starting point to determine the lowest-energy antiferromagnetic spin state, using the PBE density functional and light settings.

We also determine the DFT-PBE optimized lattice positions for later use as a template to create Fe$_2$O$_3$ cluster structures, which will also be handled (in this tutorial) using the same density functional and settings.

Finally, we use the DFT-PBE pre-relaxed bulk structure and approximate Hessian matrix as a starting point to calculate the optimized structure, as well as energy band structure and density of states, using a hybrid density functional, HSE06. The hybrid density functional would be expected to approximate certain aspects of the system better than the semilocal density functional PBE, particularly the energy band gap and spin moment of the Fe ions.

All calculations will here be based on "light" settings for the sake of computational feasibility within a tutorial. In a calculation for production purposes, we would recommend "intermediate" settings for a bulk oxide.

Here is an additional remark related to the choice of FHI-aims settings specifically for dense solids and "tight" settings. In principle, it would seem reasonable to post-converge results for a bulk oxides with "tight" settings. This could be done; however, it turns out that the default "tight" basis set especially for oxygen is already rather large, as is the "tight" basis set for Fe. In a relatively sparse structure and for relatively weak interactions (e.g., a Fe ion complexated by a molecule), "tight" settings might still be a reasonable choice. For a dense oxide, however, the density of basis functions per volume becomes very high, i.e., the basis set is close to the (over)converged limit in this case. Such calculations will tend to become fairly expensive. In earlier simulations of bulk oxides, we have found that "intermediate" settings at least for oxygen are a perfectly acceptable choice, at significantly cheaper cost than a "tight" basis set.

## Step 1: Pre-Relaxation with PBE, light settings

As mentioned in Part 1 of this tutorial:

- We initialise the Fe ions in the `geometry.in` file in their formal charge state (+3) and in their expected high-spin state (+5 or -5), using the antiferromagnetic arrangement found previously in the literature. 
- Correspondingly, we set `initial charge -2.` keywords for each O atom in the structure.

```
lattice_vector 2.5170999999999992 1.4532483625772064 4.5839666666666661 
lattice_vector -2.5170999999999992 1.4532483625772064 4.5839666666666661 
lattice_vector 0.0000000000000000 -2.9064967251544127 4.5839666666666661 
atom_frac 0.1450200000000000 0.1450200000000000 0.1450200000000000 Fe
    initial_charge 3.
    initial_moment 5.
atom_frac 0.3549800000000000 0.3549800000000000 0.3549800000000000 Fe
    initial_charge 3.
    initial_moment -5.
atom_frac 0.6450200000000000 0.6450200000000000 0.6450200000000000 Fe
    initial_charge 3.
    initial_moment 5.
atom_frac 0.8549800000000000 0.8549800000000000 0.8549800000000000 Fe
    initial_charge 3.
    initial_moment -5.
atom_frac 0.5582000000000001 0.9418000000000000 0.2499999999999999 O
    initial_charge -2.
atom_frac 0.2500000000000000 0.5581999999999998 0.9418000000000000 O
    initial_charge -2.
atom_frac 0.9417999999999999 0.2500000000000001 0.5581999999999998 O
    initial_charge -2.
atom_frac 0.4418000000000000 0.0582000000000001 0.7499999999999999 O
    initial_charge -2.
atom_frac 0.7500000000000001 0.4418000000000000 0.0582000000000000 O
    initial_charge -2.
atom_frac 0.0581999999999997 0.7500000000000001 0.4418000000000001 O
    initial_charge -2.
```

Tasks:

1. Pre-relax the structure with light species_default settings. (Refer to [Tutorial Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) regarding how to run the FHI-aims code for given `geometry.in` and `control.in` files.)

You will need to prepare a `control.in` file for this purpose. There are several options to do this - see [Tutorial Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) for the meaning of some of the basic keywords. 

Using a text editor, you could create a control.in file including the following keywords:

    xc                                 pbe
    spin                               collinear
    relativistic                       atomic_zora scalar
    k_grid                             8 8 8
    relax_geometry                     trm 5e-3
    relax_unit_cell                    full

Additionally, append the "light" species defaults for O and for Fe as described in [Tutorial Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/).

There are two important keywords here:

First, we do need to request specifically that the unit cell vectors be included as variable parameters in the total-energy minimization with respect to the atomic positions, i.e.

    relax_unit_cell                    full

Second, any periodic calculation will require a k-space grid to discretize reciprocal space and facilitate integrals in reciprocal space. The chosen discretization is

    k_grid                             8 8 8

meaning that the unit cell of reciprocal space will be discretized using a three-dimensional grid of k-values that is parallel to the three reciprocal-space lattice vectors, with eight grid spacings each along each reciprocal lattice vector. 

How do we know that this discretization is sufficiently dense? In principle, we should test the k-space discretization for numerical convergence. As a starting point (used here), a useful rule of thumb is that the k-space discretization value n_i corresponding to a particular real-space unit vector a_i should fulfill an approximate criterion

$$
n_i * a_i > 40 Å
$$

All three real-space unit vectors of the primitive unit cell of Hematite Fe$_2$O$_3$ have an approximate length of 5.42 Å, i.e., our chosen k-space grid fulfills this rule of thumb. 

In a publishable simulation, we would likely want to use a k-space grid with better convergence for final results, or at least a k-space grid that is better tested.

We could, very similarly, also construct a `control.in` file using [**GIMS**](https://gims.ms1p.org/static/index.html). This choice has the additional advantage that the Brillouin zone of the material is visualized during the construction of the input files.

We could, finally, use the clims command-line tool `clims-prepare-run --relax` to obtain a template for the control.in file. 

2. Analyze the results using [**GIMS**](https://gims.ms1p.org/static/index.html). 

3. One thing that we are missing is a deeper analysis of the final, self-consistent spin state of the system, in an atom-resolved manner. This state does not necessarily have to be the same as the initially configured spin state. 

A measure for the overall spin state is the integral over the modulus of the difference of the spin densities over the entire unit cell, that is

$$
\int_{uc} d^3r |n^\uparrow(r) - n^\downarrow(r)|
$$

This quantity is printed in every s.c.f. iteration of a spin-polarized FHI-aims calculation. To obtain the final value from the converged FHI-aims standard output file at the command line, type

```
grep rho_up-rho_dn aims.out | tail -1
```
 
This command will reveal the overall integrated polarization of the system.

A qualitative atom-resolved value for the spin configuration of the Fe ions could also be obtained using a Mulliken analysis, i.e., a projection of atomic states and densities onto individual atomic-like components, using basis functions as a projector.

A Mulliken analysis, or better, analysis in terms of a projected density of states (which we did not include here), could be activated using the keyword
```
output mulliken
```

in `control.in`.

The analysis of spin state and of the projected density of states is provided for a different, likely better density functional (HSE06) in Part 3. 

For a "light" DFT-PBE pre-relaxation, a Mulliken analysis would still be useful as a consistency check and diagnostic output, but it would have to be verified by a more precise higher-level calculation.

## Step 2: Final-Relaxation with HSE06, light settings

Semilocal density functionals (such as PBE) can have severe problems in transition metal oxides, particularly in cases in which the individual transition metal ions are not formally in a high-spin state. We skirted this problem with our choice of Fe$_2$O$_3$, which places the Fe(III) ion in its highest possible spin state in the ground state.

Nevertheless, for more precise calculations (including band structures and densities of states), a hybrid density functional would be the better choice. 

We here use the HSE06 density functional to follow up on our DFT-PBE pre-relaxation. 

FHI-aims actually does include a fairly capable, linear-scaling hybrid density functional theory implementation for non-periodic and periodic systems. This implementation has been used to compute spin-orbit coupled band structures (intermediate settings) for periodic structure sizes above 1,500 atoms including heavy elements, without making any significant approximations to the HSE06 functional or other settings along the way.

Nevertheless, hybrid DFT is still significantly more expensive than semilocal DFT, both in CPU time and in memory consumption. This increase in resources is particularly significant for dense solids, in which a high density of basis functions per volume exists and overlaps. For Fe$_2$O$_3$, this creates somewhat of a computational challenge.

For the purposes of this tutorial, we therefore use "light" settings for the DFT-HSE06 post-relaxation. The output file available in the `solutions` folder shows that this calculation (full relaxation including unit cell) completes in a somewhat reasonable wall clock time (just under half an hour) on a reasonably large computer (here, 216 current Intel CPU cores). 

Usually, we would want to perform this simulation with "intermediate" settings.

During this tutorial, you may want to try the calculation or you may simply wish to look at the existing output file. The steps to perform the simulation are:

1. Create a new folder for the HSE06 relaxation. We will use the `geometry.in.next_step` file from the PBE relaxation to begin the HSE06 relaxation. Note that this file does not contain the `initial_charge` and `initial_moment` values as there is no exact atomic spin decomposition possible with the converged density. One could manually add this after copying `geometry.in.next_step` from the PBE folder to a `geometry.in` file in the HSE06 folder, however a simpler solution is to use the `clims-reinitialize-geometry` tool. Inside the PBE folder, run the command:
```
clims-reinitialize-geometry geometry.in geometry.in.next.step --scaled
```
This will create a new file, `geometry-reinitialized.in` with the geometry from `geometry.in.next.step`, but the `initial_charge` and `initial_moment` values from `geometry.in`. The `--scaled` option gives fractional atomic coordinates in the `geometry-reinitialized.in` file. We can now move this file into the HSE06 directory, and rename it `geometry.in`. Also, copy over the `hessian.aims` file from the PBE directory to the HSE06 one.
2. Prepare the input file `control.in` for the final relaxation with HSE06. The `control.in` should contain the following, important lines to change the density functional to HSE06 with a screening parameter of 0.11 (Bohr radii)$^{-1}$ and its (default) exchange mixing parameter of 0.25:
    ```
    xc hse06 0.11
    hse_unit bohr
    hybrid_xc_coeff 0.25
    ```

Alternatively, the modification can be accomplished using [**GIMS**](https://gims.ms1p.org/static/index.html).
  
Finally, you could use (again) ``clims-prepare-run --relax --hse06`` to create the template for the `control.in` file. 


3. Please also (manually) add the following line to the `control.in` file:
 
    ```
    elsi_restart read_and_write 100
    ```

This keyword will write the converged density matrices for all k-points to disk (unfortunately, a large number of such files for a dense solid).

The parameter `100` implies that the density matrices will only be written after every 100 s.c.f. cycles or for the fully s.c.f.-converged density matrix. We can later reuse this converged density matrix to restart the calculation for the converged atomic positions and save some time for postprocessing tasks, i.e., to generate output such as energy band structures, full or partial densities of states, etc.
    
All solutions to this tutorial can be again found in the `solution` folder. Have fun!
